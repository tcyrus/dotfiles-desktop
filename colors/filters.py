#!/usr/bin/env python

def filter_strip(color):
	return color.lstrip('#')


def filter_hex(color, prefix='0x'):
	return prefix + filter_strip(color)


def filter_rgb(color, sep=','):
	color = filter_strip(color)
	rgb = tuple(str(int(color[i: i+2], 16)) for i in (0, 2, 4))
	return sep.join(rgb)


def filter_xrgba(color):
    color = filter_strip(color).lower()
    return "%s%s/%s%s/%s%s/ff" % (*color,)
