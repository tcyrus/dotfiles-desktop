# dotfiles (for desktop)

Uses GNU Stow for management (except for `wallpapers` and some others). I try to cite my
sources to the best of my ability, but sometimes I can't figure out where I find these things.

## External Resources
* `polybar`
  * [`polybar-scripts`](https://github.com/x70b1/polybar-scripts)
