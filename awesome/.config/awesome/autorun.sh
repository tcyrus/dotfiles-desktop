#!/bin/sh

function run {
  if ! pgrep $1 ;
  then
    $@&
  fi
}

run compton -b --no-fading-openclose