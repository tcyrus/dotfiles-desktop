-------------------------------
--  "Custom" awesome theme   --
--   By Tim Cyrus (tcyrus)   --
-------------------------------

-- Alternative icon sets and widget icons:
--  * http://awesome.naquadah.org/wiki/Nice_Icons

-- {{{ Main
theme = {}
theme.tasklist_disable_icon = true
theme.wallpaper = "~/.files/wallpapers/Pictures/wallpapers/city_durarara.jpg"
theme.wallpaper_mode = 'tiled'
-- }}}

-- {{{ Styles
theme.font = "DejaVuSansMono Nerd Font Mono 8"

-- {{{ Colors
theme.fg_normal  = "#a0a19f"
theme.fg_focus   = "#fff"
theme.fg_urgent  = "#cf9"
theme.bg_normal  = "#030403"
theme.bg_focus   = "#3c3c3c"
theme.bg_urgent  = "#484848"
theme.bg_systray = theme.bg_focus
-- }}}

-- {{{ Borders
theme.border_width  = 2
theme.border_normal = "#181818"
theme.border_focus  = "#363636"
theme.border_marked = "#363636"
-- }}}

-- {{{ Titlebars
theme.titlebar_bg_focus  = "#3F3F3F"
theme.titlebar_bg_normal = "#3F3F3F"
-- }}}

-- There are other variable sets
-- overriding the default one when
-- defined, the sets are:
-- [taglist|tasklist]_[bg|fg]_[focus|urgent]
-- titlebar_[normal|focus]
-- tooltip_[font|opacity|fg_color|bg_color|border_width|border_color]
-- Example:
--theme.taglist_bg_focus = "#CC9393"
-- }}}

-- {{{ Widgets
-- You can add as many variables as
-- you wish and access them by using
-- beautiful.variable in your rc.lua
--theme.fg_widget        = "#AECF96"
--theme.fg_center_widget = "#88A175"
--theme.fg_end_widget    = "#FF5656"
--theme.bg_widget        = "#494B4F"
--theme.border_widget    = "#3F3F3F"
-- }}}

-- {{{ Mouse finder
theme.mouse_finder_color = "#CC9393"
-- mouse_finder_[timeout|animate_timeout|radius|factor]
-- }}}

-- {{{ Menu
-- Variables set for theming the menu:
-- menu_[bg|fg]_[normal|focus]
-- menu_[border_color|border_width]
theme.menu_height = 15
theme.menu_width  = 100
-- }}}

-- {{{ Icons
-- {{{ Taglist
theme.taglist_squares_sel   = "~/.config/awesome/themes/amadeus/taglist/squarefz.png"
theme.taglist_squares_unsel = "~/.config/awesome/themes/amadeus/taglist/squarez.png"
--theme.taglist_squares_resize = "false"
-- }}}

-- {{{ Misc
theme.awesome_icon           = "~/.config/awesome/themes/amadeus/awesome-icon.png"
theme.menu_submenu_icon      = "~/.config/awesome/themes/amadeus/submenu.png"
-- }}}

-- {{{ Layout
theme.layout_tile       = "~/.config/awesome/themes/amadeus/layouts/tile.png"
theme.layout_tileleft   = "~/.config/awesome/themes/amadeus/layouts/tileleft.png"
theme.layout_tilebottom = "~/.config/awesome/themes/amadeus/layouts/tilebottom.png"
theme.layout_tiletop    = "~/.config/awesome/themes/amadeus/layouts/tiletop.png"
theme.layout_fairv      = "~/.config/awesome/themes/amadeus/layouts/fairv.png"
theme.layout_fairh      = "~/.config/awesome/themes/amadeus/layouts/fairh.png"
theme.layout_spiral     = "~/.config/awesome/themes/amadeus/layouts/spiral.png"
theme.layout_dwindle    = "~/.config/awesome/themes/amadeus/layouts/dwindle.png"
theme.layout_max        = "~/.config/awesome/themes/amadeus/layouts/max.png"
theme.layout_fullscreen = "~/.config/awesome/themes/amadeus/layouts/fullscreen.png"
theme.layout_magnifier  = "~/.config/awesome/themes/amadeus/layouts/magnifier.png"
theme.layout_floating   = "~/.config/awesome/themes/amadeus/layouts/floating.png"
-- }}}

-- {{{ Titlebar
theme.titlebar_close_button_focus  = "~/.config/awesome/themes/amadeus/titlebar/close_focus.png"
theme.titlebar_close_button_normal = "~/.config/awesome/themes/amadeus/titlebar/close_normal.png"

theme.titlebar_ontop_button_focus_active  = "~/.config/awesome/themes/amadeus/titlebar/ontop_focus_active.png"
theme.titlebar_ontop_button_normal_active = "~/.config/awesome/themes/amadeus/titlebar/ontop_normal_active.png"
theme.titlebar_ontop_button_focus_inactive  = "~/.config/awesome/themes/amadeus/titlebar/ontop_focus_inactive.png"
theme.titlebar_ontop_button_normal_inactive = "~/.config/awesome/themes/amadeus/titlebar/ontop_normal_inactive.png"

theme.titlebar_sticky_button_focus_active  = "~/.config/awesome/themes/amadeus/titlebar/sticky_focus_active.png"
theme.titlebar_sticky_button_normal_active = "~/.config/awesome/themes/amadeus/titlebar/sticky_normal_active.png"
theme.titlebar_sticky_button_focus_inactive  = "~/.config/awesome/themes/amadeus/titlebar/sticky_focus_inactive.png"
theme.titlebar_sticky_button_normal_inactive = "~/.config/awesome/themes/amadeus/titlebar/sticky_normal_inactive.png"

theme.titlebar_floating_button_focus_active  = "~/.config/awesome/themes/amadeus/titlebar/floating_focus_active.png"
theme.titlebar_floating_button_normal_active = "~/.config/awesome/themes/amadeus/titlebar/floating_normal_active.png"
theme.titlebar_floating_button_focus_inactive  = "~/.config/awesome/themes/amadeus/titlebar/floating_focus_inactive.png"
theme.titlebar_floating_button_normal_inactive = "~/.config/awesome/themes/amadeus/titlebar/floating_normal_inactive.png"

theme.titlebar_maximized_button_focus_active  = "~/.config/awesome/themes/amadeus/titlebar/maximized_focus_active.png"
theme.titlebar_maximized_button_normal_active = "~/.config/awesome/themes/amadeus/titlebar/maximized_normal_active.png"
theme.titlebar_maximized_button_focus_inactive  = "~/.config/awesome/themes/amadeus/titlebar/maximized_focus_inactive.png"
theme.titlebar_maximized_button_normal_inactive = "~/.config/awesome/themes/amadeus/titlebar/maximized_normal_inactive.png"
-- }}}
-- }}}
theme.menubar_disable_icon = true


return theme
