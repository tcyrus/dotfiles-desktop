//SETTINGS
//After adjusting, save file and refresh the skin to see effect.

[Variables]

//LOCATION FOR WEATHER FUNCTION
//WeatherLocation=

//12HR or 24HR CLOCK
//change value to '0' for 12hr and '1' for 24hr
ClockHours=0

//GAME STYLE
//'0' for show clock and '1' for hide clock
GameStyle=0

//TEMPERATURE UNIT
//'0' for Celsius and '1' for Fahrenheit 
CorF=1
CorF2=f

//STYLESHEET
LoadStyle=P4
LoadLang=English

//WOEID
WID=12180
