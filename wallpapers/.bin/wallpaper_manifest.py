#!/usr/bin/env python3

import boto3
import json
import os

minio_sess = boto3.Session(profile_name='minio')
s3 = minio_sess.resource('s3', endpoint_url='https://minio.tcyr.us')

bucket = s3.Bucket('walls')

def manifest2metadata(manifest):
    for obj_s in bucket.objects.all():
        manifest_obj = manifest.get(obj_s.key)
        if manifest_obj is not None:
            obj = obj_s.Object()

            entry = {}

            if 'Manifest-Link-Data' not in obj.metadata:
                entry['Manifest-Link-Data'] = json.dumps(manifest_obj['links'], sort_keys=True)

            if 'Manifest-Tag-Data' not in obj.metadata:
                entry['Manifest-Tag-Data'] = json.dumps(sorted(manifest_obj['tags']))

            if len(entry.keys()) != 0:
                # obj.put(Metadata=entry)
                obj.metadata.update(entry)
                obj.copy_from(
                    CopySource={
                        'Bucket': obj_s.bucket_name,
                        'Key': obj_s.key
                    },
                    CopySourceIfMatch=obj.e_tag,
                    Metadata=obj.metadata,
                    MetadataDirective='REPLACE'
                )


def metadata2manifest():
    manifest = {}
    for obj_s in bucket.objects.all():
        obj = obj_s.Object()

        entry = {}

        linkData = obj.metadata.get('Manifest-Link-Data')
        if linkData is not None:
            entry['links'] = json.loads(linkData)

        tagData = obj.metadata.get('Manifest-Tag-Data')
        if tagData is not None:
            entry['tags'] = json.loads(tagData)

        if len(entry.keys()) != 0:
            manifest[obj_s.key] = entry
    return manifest


def loadManifest(manifest_path):
    manifest = {}
    with open(manifest_path) as f:
        manifest = json.load(f)
    manifest2metadata(manifest)


def dumpManifest(manifest_path):
    manifest = metadata2manifest()
    with open(manifest_path) as f:
        json.dump(manifest, f)


if __name__ == '__main__':
    # manifest_path = os.path.expanduser('~/.config/wallpaper_manifest.json')
    # loadManifest(manifest_path)
    manifest = metadata2manifest()
    print(json.dumps(manifest))
