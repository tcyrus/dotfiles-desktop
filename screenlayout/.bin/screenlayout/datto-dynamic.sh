#!/bin/sh

set -e

xrandr --output eDP1 --primary --mode 1920x1080 --pos 0x0 --rotate normal

D2_STATUS=$(</sys/class/drm/card0-DP-5/status)
D3_STATUS=$(</sys/class/drm/card0-DP-6/status)

if [[ "$D2_STATUS" == "connected" ]]; then
xrandr --output DP2-1 --mode 1920x1080 --pos 1920x0 --rotate normal
else
xrandr --output DP2-1 --off
fi

if [[ "$D3_STATUS" == "connected" ]]; then
xrandr --output DP2-2 --mode 1920x1080 --pos 3840x0 --rotate normal
else
xrandr --output DP2-2 --off
fi

xrandr --output VIRTUAL1 --off \
       --output DP1 --off \
       --output DP2-3 --off \
       --output HDMI2 --off \
       --output HDMI1 --off \
       --output DP2 --off
